#!/usr/bin/env bash

bash --login '/Applications/Docker/Docker Quickstart Terminal.app/Contents/Resources/Scripts/start.sh' <<DOCKER_COMMANDS

docker-compose -f docker-compose.yml up -d

DOCKER_COMMANDS
